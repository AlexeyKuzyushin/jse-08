package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface ITerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String DESCR_HELP = " - Display terminal commands.";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String DESCR_VERSION = " - Show version info.";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String DESCR_ABOUT = " - Show developer info.";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String DESCR_INFO = " - Show system info";

    String CMD_EXIT = "exit";

    String DESCR_EXIT = " - Close application";
}
