package ru.rencredit.jschool.kuzyushin.tm.model;

import ru.rencredit.jschool.kuzyushin.tm.constant.ITerminalConst;

public class TerminalCommand {

    public static final TerminalCommand HELP = new TerminalCommand(
            ITerminalConst.CMD_HELP, ITerminalConst.ARG_HELP, ITerminalConst.DESCR_HELP
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            ITerminalConst.CMD_ABOUT, ITerminalConst.ARG_ABOUT, ITerminalConst.DESCR_ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            ITerminalConst.CMD_VERSION, ITerminalConst.ARG_VERSION, ITerminalConst.DESCR_VERSION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            ITerminalConst.CMD_INFO, ITerminalConst.ARG_INFO, ITerminalConst.DESCR_INFO
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            ITerminalConst.CMD_EXIT, null, ITerminalConst.DESCR_EXIT
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }
}
